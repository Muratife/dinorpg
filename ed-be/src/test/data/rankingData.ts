import { PlayerRanking, Ranking } from '../../models';
import { player } from '../utils/constants';

export const playerRanking = {
	dinozCount: 2,
	sumPointsDisplayed: 5,
	sumPoints: 5,
	averagePointsDisplayed: 4,
	averagePoints: 4,
	player: { name: 'Biocat' }
} as Ranking;

export const playersRankingSum = [
	{
		dinozCountDisplayed: 7,
		sumPointsDisplayed: 14,
		player: { name: 'Biocat' },
		playerId: 1,
		averagePointsDisplayed: 2,
		sumPosition: 1
	},
	{
		dinozCountDisplayed: 4,
		sumPointsDisplayed: 12,
		player: { name: 'Jolu' },
		playerId: 1,
		averagePointsDisplayed: 3,
		sumPosition: 2
	}
] as Array<Ranking>;

export const playersToUpdatePoints = [
	{
		dinozCount: 7,
		sumPoints: 11,
		playerId: player.id_1,
		averagePoints: 2
	},
	{
		dinozCount: 4,
		sumPoints: 12,
		playerId: player.id_2,
		averagePoints: 3
	}
] as Array<Ranking>;

export const playersRankingAverage = [
	{
		dinozCountDisplayed: 7,
		sumPointsDisplayed: 14,
		player: { name: 'Biocat' },
		playerId: 1,
		averagePointsDisplayed: 2,
		sumPosition: 2
	},
	{
		dinozCountDisplayed: 4,
		sumPointsDisplayed: 12,
		player: { name: 'Jolu' },
		playerId: 1,
		averagePointsDisplayed: 3,
		sumPosition: 1
	}
] as Array<Ranking>;

export const playersRankingSumResult = [
	{
		dinozCount: 7,
		pointCount: 14,
		playerName: 'Biocat',
		playerId: 1,
		pointAverage: 2,
		position: 1
	},
	{
		dinozCount: 4,
		pointCount: 12,
		playerName: 'Jolu',
		playerId: 1,
		pointAverage: 3,
		position: 2
	}
] as Array<PlayerRanking>;

export const playersRankingAverageResult = [
	{
		dinozCount: 7,
		pointCount: 14,
		playerName: 'Biocat',
		playerId: 1,
		pointAverage: 2,
		position: 2
	},
	{
		dinozCount: 4,
		pointCount: 12,
		playerName: 'Jolu',
		playerId: 1,
		pointAverage: 3,
		position: 1
	}
] as Array<PlayerRanking>;
