export enum SkillType {
	E = 'E',
	P = 'P',
	S = 'S',
	A = 'A',
	U = 'U',
	I = 'I',
	C = 'C'
}
