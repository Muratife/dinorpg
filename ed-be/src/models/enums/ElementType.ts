export enum ElementType {
	FIRE = 'fire',
	WATER = 'water',
	WOOD = 'wood',
	LIGHT = 'light',
	AIR = 'air',
	VOID = 'void'
}
